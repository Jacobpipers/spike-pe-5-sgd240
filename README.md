# Spike Report

## SPIKE PE 5 - Constraints

### Introduction

We want to understand the capabilities of the Physics Engine that Unreal uses. We’ve looked at basic physics interactions. It’s time to understand the next step, constraints.

### Goals

* The Spike Report should answer each of the Gap questions Building on Spike PE-4, although the following targets should not be destructible:

* Build a Constraint-Actor based “target”:
  * Create an archery-style “target”, which is connected by a Constraint Actor to a Box, and hangs mid-air.
  * The target should be simulating, and the box should be held stable (either not-simulating, or locked in place somehow).

* Build a Constraint-Component based “target”:
  * Same as above, but using components instead of actors.

* Build a small demo level to show off the various constraints that can be built:
  * Linear limits, angular limits, linear motors, and angular motors
  * Build at least one breakable constraint

### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* Visual Studio
* Unreal Engine

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

* Follow this Guide - [Constraints Actor](https://docs.unrealengine.com/latest/INT/Engine/Physics/Constraints/ConstraintsUserGuide/)
  * Add A Box in the World and Grab the Target blueprint in the scene. As Seen In Image below

    ![](ConstraintActor.png)

    * Set the Constraint Actor 1 to cube (Stationary object) and second one to ArcherTarget

    ![](ConstraintActorChoices.png)

* Follow this Guide - [Constraints-Component](https://docs.unrealengine.com/latest/INT/Engine/Physics/Constraints/ConstraintsBlueprints/index.html)

* Limits Types / breakable constraint

    * [Linear Limits](https://docs.unrealengine.com/latest/INT/Engine/Physics/Constraints/ConstraintsReference/index.html#linearlimits)

      * Set the Linear limits to ZMotion to limited the others to locked. Set Angular limits to locked.

    * [Angular Limits](https://docs.unrealengine.com/latest/INT/Engine/Physics/Constraints/ConstraintsReference/index.html#angularlimits)

      * Set the Angular 2Swing motion to limited the others to locked. Set Linear limits to locked As Seen Image below.

        ![](AngularLimit.png)

* Constraint Motors

    * [Linear Motor](https://docs.unrealengine.com/latest/INT/Engine/Physics/Constraints/ConstraintsReference/index.html#linearmotor) With Linear Drive Acts as a spring trying to get back to resting state.

      * Set Zmotion to Limited. Set Linear Motor as seen in picture.

        ![](LinMotor.png)

    * [Angular Motor](https://docs.unrealengine.com/latest/INT/Engine/Physics/Constraints/ConstraintsReference/index.html#angularmotor)

      * Set Angler Drive mode to Twist and swing as seen in picture below.

        ![](AngMotor.png)

### What we found out

Skill: How to effectively utilise Constraints.

Knowledge: What are Physics Constraints?

A constraint is a type of joint. It allows you to connect two Actors together with at least one physically simulating. being able to apply limits or forces. [See More](https://docs.unrealengine.com/latest/INT/Engine/Physics/Constraints/ConstraintsUserGuide/index.html)


What Constraints and Motors are?

In Unreal The physics engine has constraints and motors. 

A Constraints is a relation between two Cordinetes. This is like a restriction or freedom of movement between diffrent points [see more](https://en.wikipedia.org/wiki/Constraint_(classical_mechanics)) in Space That can move within a certain limit of distance. 

In Unreal there is Angular and Linear Constraints. 

Within Constraints A Motor moves the object along a axis or axes ussing forces. Within the Constraints limitations. There is Linear and Angular motors in unreal. Linear motor act as spring and Angular motor spins like a clog motor within the limits. 

[Angular Motor](https://docs.unrealengine.com/latest/INT/Resources/ContentExamples/Physics/1_6/)
has a Slerp and a Twist and Swing behaviour. 

[Linear Motor](https://docs.unrealengine.com/latest/INT/Resources/ContentExamples/Physics/1_7/index.html)

### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.