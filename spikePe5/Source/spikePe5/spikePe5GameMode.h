// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "spikePe5GameMode.generated.h"

UCLASS(minimalapi)
class AspikePe5GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AspikePe5GameMode();
};



